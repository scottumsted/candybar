__author__ = 'scottumsted'

import Image,ImageDraw
import cStringIO

class CandyBarImage:

    width = 400
    height = 60
    imageByteArray = []
    imageType = "PNG"
    currentPosition = 0
    image = None
    draw = None

    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.setup()

    def setup(self):
        self.image = Image.new('RGBA',(self.width,self.height),'white')
        self.draw = ImageDraw.Draw(self.image)

    def addBar(self, barWidth):
        d = (self.currentPosition,0,self.currentPosition+barWidth,self.height+1)
        self.draw.rectangle(d,'black')
        self.currentPosition += barWidth

    def addSpace(self, spaceWidth):
        d = (self.currentPosition,0,self.currentPosition+spaceWidth,self.height+1)
        self.draw.rectangle(d,'white')
        self.currentPosition += spaceWidth

    def rescale(self, scaledWidth, scaledHeight):
        return self.image.resize((scaledWidth,scaledHeight),Image.NEAREST)

    def scaleAndConvertToByteArray(self, scaledWidth, scaledHeight):
        return self._convert(self.rescale(scaledWidth,scaledHeight))

    def convertToByteArray(self):
        self.imageByteArray = self._convert(self.image)
        return self.imageByteArray

    def _convert(self, renderImage):
        fp = cStringIO.StringIO()
        renderImage.save(fp,self.imageType)
        iba = fp.getvalue()
        fp.close()
        return iba

